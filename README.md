copyyright: Guoan Wang
Filename: main.cpp. cards.h. cards.cpp
Purpose: create a mexican-version black jack and let the user play with
         the AI. (For the purpose of the PIC10C first homework assignment).

Contact: Guoan Wang <andywang50@ucla.edu>
Mod history:  2017-01-19

Coded in Visual Studio 2015. I have not tested it on Visual Studio 2012 yet. 

I have run it in both debug and release mode. Both work well.
